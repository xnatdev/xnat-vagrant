#!/usr/bin/env python3

# Orthanc - A Lightweight, RESTful DICOM Store
# Copyright (C) 2012-2016 Sebastien Jodogne, Medical Physics
# Department, University Hospital of Liege, Belgium
# Copyright (C) 2017-2020 Osimis S.A., Belgium
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import base64
import httplib2
import json
import re
import sys

URL_REGEX = re.compile('(http|https)://((.+?):(.+?)@|)(.*)')

if len(sys.argv) != 3:
    print("""
Script to copy the content of one Orthanc server to another Orthanc
server through their REST API.

Usage: %s [SourceURI] [TargetURI]
For instance: %s http://orthanc:password@127.0.0.1:8042/ http://127.0.0.1:8043/
""" % (sys.argv[0], sys.argv[0]))
    exit(-1)


def create_headers(parsed_url):
    headers = {}
    username = parsed_url.group(3)
    password = parsed_url.group(4)

    if username is not None and password is not None:
        # This is a custom reimplementation of the
        # "Http.add_credentials()" method for Basic HTTP Access
        # Authentication (for some weird reason, this method does not
        # always work)
        # http://en.wikipedia.org/wiki/Basic_access_authentication
        auth_header = username + ':' + password
        headers['authorization'] = 'Basic ' + base64.b64encode(auth_header.encode('ascii')).decode('ascii')

    return headers


def get_base_url(parsed_url):
    return '%s://%s' % (parsed_url.group(1), parsed_url.group(5))


def do_get_string(url):
    global URL_REGEX
    parsed_url = URL_REGEX.match(url)
    headers = create_headers(parsed_url)

    h = httplib2.Http()
    resp, content = h.request(get_base_url(parsed_url), 'GET', headers=headers)

    if resp.status == 200:
        return content
    else:
        raise Exception('Unable to contact Orthanc at: ' + url)


def do_post_dicom(url, body):
    global URL_REGEX
    parsed_url = URL_REGEX.match(url)
    headers = create_headers(parsed_url)
    headers['content-type'] = 'application/dicom'

    h = httplib2.Http()
    resp, content = h.request(get_base_url(parsed_url), 'POST',
                              body=body,
                              headers=headers)

    if resp.status != 200:
        raise Exception('Unable to contact Orthanc at: ' + url)


def _decode_json(s):
    if sys.version_info >= (3, 0):
        return json.loads(s.decode())
    else:
        return json.loads(s)


def do_get_json(url):
    return _decode_json(do_get_string(url))


SOURCE = sys.argv[1]
TARGET = sys.argv[2]

for study in do_get_json('%s/studies' % SOURCE):
    print('Sending study %s...' % study)
    for instance in do_get_json('%s/studies/%s/instances' % (SOURCE, study)):
        dicom = do_get_string('%s/instances/%s/file' % (SOURCE, instance['ID']))
        do_post_dicom('%s/instances' % TARGET, dicom)
