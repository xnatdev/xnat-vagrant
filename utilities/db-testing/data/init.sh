#!/bin/bash
set -e

DEFAULT_OWNER=$(psql -v ON_ERROR_STOP=1 --username="${POSTGRES_USER}" --dbname=template1 --tuples-only --no-align --command="SELECT schema_owner FROM information_schema.schemata WHERE schema_name = 'public'")

psql -v ON_ERROR_STOP=1 --username="${POSTGRES_USER}" --dbname=template1 <<-EOSQL
-- Install trigram extension
CREATE EXTENSION pg_trgm;

-- Configure XNAT database
CREATE USER xnat CREATEDB;
ALTER USER xnat WITH PASSWORD 'xnat';
ALTER SCHEMA public OWNER to xnat;
CREATE DATABASE xnat OWNER xnat;

-- Configure Orthanc database
CREATE USER orthanc NOCREATEDB;
ALTER USER orthanc WITH PASSWORD 'orthanc';
ALTER SCHEMA public OWNER to orthanc;
CREATE DATABASE orthanc OWNER orthanc;

ALTER SCHEMA public OWNER to ${DEFAULT_OWNER};
EOSQL
