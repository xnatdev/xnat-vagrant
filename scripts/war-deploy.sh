#!/usr/bin/env bash

#
# Download and deploy XNAT war
#

sourceScript() {
    test -f /vagrant/scripts/$1 && source /vagrant/scripts/$1 || source /vagrant-root/scripts/$1
}

# Now initialize the build environment from the config's vars.sh settings.
source /vagrant/.work/vars.sh

# look in config's scripts folder first, then try the multi root
sourceScript defaults.sh
sourceScript bin/macros

# We're really starting this script now.
echo Now running the "war-deploy.sh" provisioning script.

if [[ -e ~/.bash_profile ]]; then
    source ~/.bash_profile;
elif [[ -e ~/.bashrc ]]; then
    source ~/.bashrc;
fi

# Stop these services so that we can configure them later.
manageService ${TOMCAT} stop
manageService nginx stop

# Make SURE Tomcat is shut down.
TOMCAT_PID=$(ps ax | fgrep ${TOMCAT} | fgrep org.apache.catalina.startup.Bootstrap | awk '{$1=$1};1' | cut -f 1 -d " ")
[[ -n ${TOMCAT_PID} ]] && { sudo kill -9 ${TOMCAT_PID}; sudo rm /var/run/${TOMCAT}.pid; }

# Configure the host settings.
echo -e "${VM_IP} ${HOST} ${SERVER}" | sudo tee --append /etc/hosts

configureNginx

echo "Starting nginx..."
manageService nginx start

# TOMCAT STUFF
CURRENT_USER=$(whoami)
setFolderOwner ${CURRENT_USER} /var/lib/${TOMCAT}

setTomcatConfiguration

# Remove any existing web apps
rm -rf /var/lib/${TOMCAT}/webapps/*

# Set up PostgreSQL
configurePostgreSQL

# XNAT STUFF

# Create project subfolders

# setup XNAT data folders
setupFolders ${DATA_ROOT} ${CURRENT_USER}

# make sure there's a 'universal' local/downloads folder
#mkdir -p /vagrant-root/local/downloads
DL_DIR=/vagrant
ROOT_DL_DIR=/vagrant-root/local/downloads

# Download pre-built .war file and copy to tomcat webapps folder
getWar(){

    URL=$1

    cd ${DATA_ROOT}/src

    local TARGET_WAR="/var/lib/${TOMCAT}/webapps/${CONTEXT}.war"

    # if the file has already been downloaded to the host, use that
    if [[ -f ${DL_DIR}/${URL##*/} && ${URL##*/} == *.war ]]; then
        cp ${DL_DIR}/${URL##*/} ${TARGET_WAR}
    elif [[ -f ${ROOT_DL_DIR}/${URL##*/} && ${URL##*/} == *.war ]]; then
        cp ${ROOT_DL_DIR}/${URL##*/} ${TARGET_WAR}
    else
        cd ${DL_DIR}
        echo
        echo "Downloading: ${URL}"
        curl -L --retry 5 --retry-delay 5 -s -O ${URL} \
        && cp ${DL_DIR}/${URL##*/} ${TARGET_WAR} \
        || echo "Error downloading '${URL}'"
        cd -
    fi
}

# get the war file and copy it into the webapps folder
if [[ ! -z ${XNAT_URL} ]]; then
#    echo
    echo Getting XNAT war file...
    getWar ${XNAT_URL}
else
    echo "XNAT war file will need to be manually deployed."
fi


getPipeline() {

    URL=$1

    cd ${DATA_ROOT}/src

    [[ ! -d pipeline ]] && { mkdir pipeline; }
    cd pipeline

    # if the file has already been downloaded to the host, use that
    if [[ ! -f ${DL_DIR}/${URL##*/} ]]; then
        cd ${DL_DIR}
        echo
        echo "Downloading: ${URL}"
        curl -L --retry 5 --retry-delay 5 -s -O ${URL} \
        || echo "Error downloading '${URL}'"
        cd -
    fi

    if [[ -f ${DL_DIR}/${URL##*/} ]]; then
        echo Extracting ${URL##*/}...
        unzip -qo ${DL_DIR}/${URL##*/} || { echo "Error unzipping ${URL##*/}"; return; }
        for DIR in xnat-pipeline*; do
            find ${DIR} -mindepth 1 -maxdepth 1 -exec mv '{}' . \;
            rmdir ${DIR}
        done
        replaceTokens pipeline.gradle.properties | tee gradle.properties
        [[ -f ./gradlew ]] && sudo ./gradlew -q
    fi
}

# Get the pipeline zip file, extract it, and run the installer.
if [[ ! -z ${PIPELINE_URL} ]]; then
    #echo
    echo Getting Pipeline zip file...
    getPipeline ${PIPELINE_URL}
else
    echo "XNAT pipeline engine will need to be manually installed."
fi

# Is the variable MODULES defined?
[[ -v MODULES ]] \
    && { echo Found MODULES set to ${MODULES}, pulling repositories.; /vagrant-root/scripts/pull_module_repos.rb ${DATA_ROOT}/modules ${MODULES}; } \
    || { echo No value set for the MODULES configuration, no custom functionality will be included.; }

# Search for any post-build execution folders and execute the install.sh
for POST_DIR in /vagrant/post_*; do
    if [[ -e ${POST_DIR}/install.sh ]]; then
        echo Executing post-processing script ${POST_DIR}/install.sh
        bash ${POST_DIR}/install.sh
    fi
done

sudo rm -rf /var/log/${TOMCAT}/*

# Lastly, see if there's a public key configured.
if [[ ! -z ${PUBLIC_KEY} ]]; then
    # If so, add it to both Vagrant and XNAT users
    echo Adding public key to authorized keys for ${CURRENT_USER} and ${XNAT_USER}
    addAuthorizedPublicKey ~/.ssh/authorized_keys
    addAuthorizedPublicKey ${XNAT_HOME}/.ssh/authorized_keys
fi

# Set ownership of data and Tomcat folders to the XNAT user.
setFolderOwner ${XNAT_USER} /data ${DATA_ROOT} /var/lib/${TOMCAT}

# Run any exec scripts the user's added.
[[ -d /vagrant/exec ]] && {
    for SCRIPT in $(find /vagrant/exec -type f -executable); do
        ${SCRIPT}
    done
}

sudo chmod 755 /var/log/${TOMCAT}

checkForExtensionScripts

echo "Enabling and starting Tomcat..."
toggleService ${TOMCAT} on
startTomcat
monitorTomcatStartup
STATUS=${?}

showNotification

deployCompleteBanner ${STATUS}
exit ${STATUS}

