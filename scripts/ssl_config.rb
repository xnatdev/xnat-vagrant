class CertificateAndPrivateKey
    def initialize(certificate, key)
        @certificate = certificate
        @key = key
        @has_cert = !@certificate.to_s.empty?
        @has_key = !@key.to_s.empty?
    end

    attr_reader :certificate
    attr_reader :key

    def should_resolve
        @has_cert && @has_key
    end

    def resolve(config, cache_dir)
        found_cert = @has_cert && File.exists?(@certificate)
        found_key = @has_key && File.exists?(@key)
        found_all = found_cert && found_key

        if found_all
            Dir.mkdir(cache_dir) unless File.exists?(cache_dir)
            FileUtils.cp(@certificate, cache_dir)
            FileUtils.cp(@key, cache_dir)
            @certificate = "/vagrant-root/local/#{config}/#{File.basename(@certificate)}"
            @key = "/vagrant-root/local/#{config}/#{File.basename(@key)}"
            return true
        elsif @has_cert ^ @has_key
            puts "You've specified an SSL #{@has_cert ? 'certificate' : 'key'} but no corresponding #{!@has_cert ? 'certificate' : 'key'}. You must specify both. Generating default"
            puts "self-signed certificate and key, but you may want to try again."
        elsif @has_cert && @has_key
            if !found_cert && !found_key
                puts "You specified a certificate and key to be installed for your XNAT application, but I can't "
                puts "find the specified files #{certificate} and #{key}."
            elsif !found_cert
                puts "You specified a certificate and key to be installed for your XNAT application, but I can't "
                puts "find the specified file #{certificate}."
            else
                puts "You specified a certificate and key to be installed for your XNAT application, but I can't "
                puts "find the specified file #{key}."
            end
        end
        false
    end
end

class SslConfig
    def initialize(profile)
        @profile = profile
    end

    def process(cwd)
        is_secure_protocol = !(@profile['protocol'] !~ /^https/)
        is_secure_site_url = !(@profile['site_url'] !~ /^https/)
        if !is_secure_site_url && !is_secure_protocol
            puts "You've specified the http protocol and a site URL that uses http. No SSL certificate will be generated or installed."
        elsif !is_secure_site_url && is_secure_protocol
            puts "The specified site protocol is https, but you've specified an insecure site URL: #{@profile['site_url']}"
            puts "I've converted the protocol to http to match the configured site URL. If you want to run this XNAT"
            puts "deployment with https, make sure you set both the URL and protocol to use 'https'."
            puts
            @profile['protocol'] = 'http'
        elsif is_secure_site_url && !is_secure_protocol
            puts "The specified site protocol is http, but you've specified a secure site URL: #{@profile['site_url']}"
            puts "I've converted the site URL to use http to match the configured protocol: #{@profile['site_url']}"
            puts "If you want to run this XNAT deployment with https, make sure you set both the URL and protocol"
            puts "to use 'https'."
            puts
            @profile['site_url'].sub! 'https:', 'http:'
        else
            @profile['ssl_cert'] ||= ''
            @profile['ssl_key'] ||= ''
            @profile['ssl_ca_pem'] ||= ''
            @profile['ssl_ca_key'] ||= ''

            cache_dir = "#{cwd}/../../local/#{@profile['name']}"

            cert_and_key = CertificateAndPrivateKey.new(@profile['ssl_cert'], @profile['ssl_key'])
            ca_pem_and_key = CertificateAndPrivateKey.new(@profile['ssl_ca_pem'], @profile['ssl_ca_key'])
            should_resolve_cert_and_key = cert_and_key.should_resolve
            should_resolve_ca_pem_and_key = ca_pem_and_key.should_resolve
            if should_resolve_cert_and_key && should_resolve_ca_pem_and_key
                puts "You have configured XNAT Vagrant to use HTTPS with a predefined certificate authority and SSL"
                puts "certificate. These will be installed in your new VM appropriately."
                puts ""
            else
                if !should_resolve_cert_and_key && !should_resolve_ca_pem_and_key
                    puts "You have configured XNAT Vagrant to use HTTPS but haven't specified an SSL certificate and key,"
                    puts "so a self-signed certificate and key will be generated and installed. You can specify your own"
                    puts "certificate and key by setting the 'ssl_cert' and 'ssl_key' properties or a certificate authority"
                    puts "and key by setting the 'ssl_ca_pem' and 'ssl_ca_key' properties in local.yaml."
                    puts ""
                else
                    if should_resolve_cert_and_key && cert_and_key.resolve(@profile['name'], cache_dir)
                        @profile['ssl_cert'] = cert_and_key.certificate
                        @profile['ssl_key'] = cert_and_key.key
                    else
                        @profile.delete 'ssl_cert'
                        @profile.delete 'ssl_key'
                    end
                    if should_resolve_ca_pem_and_key && ca_pem_and_key.resolve(@profile['name'], cache_dir)
                        @profile['ssl_ca_pem'] = ca_pem_and_key.certificate
                        @profile['ssl_ca_key'] = ca_pem_and_key.key
                    else
                        @profile.delete 'ssl_ca_pem'
                        @profile.delete 'ssl_ca_key'
                    end
                end
                puts ""
                if @profile['ssl_subject'].to_s.strip.empty?
                    @profile['ssl_subject'] = "/C=US/ST=Missouri/L=Saint Louis/O=Washington University School of Medicine/OU=Neuroinformatics Research Group/CN=#{@profile['server']}"
                    puts "No subject was specified for self-signed SSL certificate generation, so the default value will be"
                    puts "used. You can provide a subject value by setting the 'ssl_subject' property in your local.yaml"
                    puts "configuration. Find out about subjects here: https://knowledge.digicert.com/solution/SO18140.html#Subject\n"
                else
                    puts "The self-signed SSL certificate will be generated using the subject:"
                    puts ""
                    puts " * #{@profile['ssl_subject']}"
                end
                puts ""
            end
        end
    end
end
