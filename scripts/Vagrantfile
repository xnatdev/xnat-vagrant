#
# XNAT Vagrant project
# http://www.xnat.org
# Copyright (c) 2015-2016, Washington University School of Medicine, all rights reserved.
# Released under the Simplified BSD license.
#

unless defined?(cfg_dir)
    puts ''
    puts '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
    puts ' Setup aborted...'
    puts ' VM setup must be done from within one of the folders in "configs".'
    puts '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
    abort('')
end

require 'yaml'
require 'optparse'

VAGRANTFILE_API_VERSION = '2'

# save all arguments to a string
all_args = ARGV.join(" ")

# print args(?)
puts "[ #{all_args} ]\n\n"

# save the root 'multi' dir
multi_root = '../..'

# puts "multi_root is #{multi_root}"

Dir.mkdir("#{cwd}/.work") unless File.exists?("#{cwd}/.work")

# load config settings
profile ||= YAML.load_file("#{cwd}/config.yaml")

# use vars.yaml if available for this config
vars_yaml = "#{cwd}/.work/vars.yaml"

# hack to force creation of vars files
# `vagrant provision setup`
if ARGV[0] != 'destroy' && ARGV[1] == 'setup'

    eval(IO.read('../../scripts/yaml_vars.rb'), binding)

    # Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
    #     # don't do anything
    # end

else

    if File.exists?(vars_yaml)

        puts "Loading existing configuration from #{vars_yaml}..."
        profile = YAML.load_file(vars_yaml)

    else

        # var processing moved to a separate file
        eval(IO.read('../../scripts/yaml_vars.rb'), binding)

    end

    shares     = profile['shares'] ||= ''
    has_shares = shares && shares != 'false' && shares != ''

    forwards     = profile['forwards'] ||= ''
    has_forwards = forwards && forwards != 'false' && forwards != ''

    is_up      = ARGV[0] == 'up'
    is_destroy = ARGV[0] == 'destroy'
    is_build   = !is_up && !is_destroy

    Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

        config.vm.define "#{profile['name']}"

        config.vm.hostname = profile['host']
        config.vm.box      = profile['box']

        # If box_url is specified, use that, otherwise use box_version
        if profile['box_url'] != ''
            config.vm.box_url       = profile['box_url']
        else
            config.vm.box_version   = profile['box_version'] ||= '>= 0'
        end

        config.vm.network 'private_network', ip: profile['vm_ip']
        if !is_destroy && has_forwards
            forwards.each { |guest_port, host_port|
                config.vm.network "forwarded_port", guest: guest_port, host: host_port
            }
        end

        config.vm.provider 'virtualbox' do |v|
            v.name   = profile['name']
            v.memory = profile['ram']
            v.cpus   = profile['cpus']
            v.gui    = profile['gui']
            v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
            v.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
            v.customize ["modifyvm", :id, "--vram", "64"]
        end

        # set the main folder as a share
        config.vm.synced_folder "#{multi_root}", '/vagrant-root', :group=> profile['vagrant'], :mount_options => ["dmode=775","fmode=664"]

        # Will only run on initial 'up'
        if profile['provision'] != ''
            # since this Vagrantfile is eval'd in the config's Vagrantfile,
            # this path is actually in the config's folder
            provision_script = "#{profile['provision']}"
            # look for provision script in config folder first
            unless File.exists?(provision_script)
                provision_script = "#{multi_root}/#{profile['provision']}"
            end
            config.vm.provision :shell, binary: true, path: provision_script
        end

        # If we're not destroying the VM, check for shared folders and mount them before trying to provision
        if !is_destroy && has_shares
            shares.each { |share, share_to|
                expanded       = File.expand_path share
                share_to_count = share_to.size
                if share_to_count == 0
                    puts "Ignoring share for #{share} because there's no target folder set."
                elsif File.exist? expanded and File.directory? expanded
                    puts "Setting up share from #{share} to #{share_to[0]}"

                    # If this is a build step there's no owner or group explicitly specified for the share...
                    if share_to.size == 1
                        target  = share_to[0]
                        options = ['dmode=755']
                        owner   = is_build ? profile['xnat_user'] : ''
                        group   = is_build ? profile['xnat_user'] : ''
                    elsif share_to.size == 2
                        # Mount options are specified.
                        target  = share_to[0]
                        options = share_to[1]
                        owner   = is_build ? profile['xnat_user'] : ''
                        group   = is_build ? profile['xnat_user'] : ''
                    elsif share_to.size == 3
                        # An owner is specified, use that for owner and group settings. NOT conditionalized by build state.
                        target  = share_to[0]
                        options = share_to[1]
                        owner   = share_to[2]
                        group   = share_to[2]
                    else
                        # An owner and group are specified, use those for owner and group settings. Anything over 4 settings we ignore.
                        # NOT conditionalized by build state.
                        target  = share_to[0]
                        options = share_to[1]
                        owner   = share_to[2]
                        group   = share_to[3]
                    end

                    if owner
                        config.vm.synced_folder expanded, target, mount_options: options, owner: owner, group: group
                    else
                        config.vm.synced_folder expanded, target, mount_options: options
                    end
                elsif File.exist? expanded
                    puts "Ignoring share for #{share} because it's not a directory."
                else
                    puts "Ignoring share for #{share} because it doesn't exist (it may be created during provisioning and mounted during reload)."
                end
            }
        end

        if is_build

            # Make the /vagrant folder owned and writeable by the VM user and group. This is important:
            # allows the xnat_user account write to the shared folder.
            config.vm.synced_folder ".", "/vagrant", :owner=> profile['xnat_user'], :group=> profile['vagrant'], :mount_options => ["dmode=775","fmode=775"]

            # If there's no 'xnat_url' property, do this
            unless profile['xnat_url']
                # Make the XNAT source folder accessible by the vagrant group
                config.vm.synced_folder profile['xnat_src'], "#{profile['data_root']}/src/xnat", :owner=> profile['xnat_user'], :group=> profile['vagrant'], :mount_options => ["dmode=775","fmode=775"]
            end

            # If there's no 'pipeline_url' property, do this
            unless profile['pipeline_url']
                unless profile['pipeline_src'].to_s.strip.empty?
                    # Make the XNAT pipeline folder accessible by the vagrant group
                    config.vm.synced_folder profile['pipeline_src'], "#{profile['data_root']}/src/pipeline", :owner=> profile['xnat_user'], :group=> profile['vagrant'], :mount_options => ["dmode=775","fmode=775"]
                end
            end

            # If this is a build operation...
            if profile['build'] != ''
                # since this Vagrantfile is eval'd in the config's own Vagrantfile,
                #
                # this path is actually in the config's folder
                build_script = "#{profile['build']}"
                # look for provision script in config folder first
                unless File.exists?(build_script)
                    build_script = "#{multi_root}/#{profile['build']}"
                end

                # Additional provisioners, called explicitly by "--provision-with foo"
                config.vm.provision 'build', type: :shell, binary: true, path: build_script, privileged: false

            end

        end

    end

end
