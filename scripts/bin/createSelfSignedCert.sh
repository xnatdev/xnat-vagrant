#!/usr/bin/env bash

SERVER=${1}
[[ -z ${2} ]] && { EMAIL="admin@miskatonic.edu"; } || { EMAIL="${2}"; }

cat > ${SERVER}.csr.cnf <<CNF
[req]
default_bits = 2048
prompt = no
default_md = sha256
distinguished_name = dn
 
[dn]
C=US
ST=Missouri
L=Saint Louis
O=Washington University School of Medicine
OU=Neuroinformatics Research Group
CN=${SERVER}
emailAddress=${EMAIL}
CNF

cat > v3-${SERVER}.ext <<V3_EXT
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = ${SERVER}
V3_EXT

openssl req -new -sha256 -nodes -out ${SERVER}.csr -newkey rsa:2048 -keyout ${SERVER}.key -config <( cat ${SERVER}.csr.cnf )
openssl x509 -req -in ${SERVER}.csr -passin pass:${SERVER} -CA rootCA-${SERVER}.pem -CAkey rootCA-${SERVER}.key -CAcreateserial -out ${SERVER}.crt -days 500 -sha256 -extfile v3-${SERVER}.ext

