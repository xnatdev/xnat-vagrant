# XNAT Vagrant Development Configuration

## Setup

This configuration sets up a virtual machine that mounts a folder containing the XNAT source code. The source code repository is
mounted in the guest VM in the folder _data_root_`/src/xnat` and the XNAT war is installed from _data_root_`/src/xnat/build/libs`.

> If you don't already have a local copy of the XNAT source, you'll need to clone or download it to your
host (local) machine. You can use the XNAT development repository as shown below or your own fork or custom repository.

```bash
git clone git@bitbucket.org:xnatdev/xnat-web.git /local/path/to/xnat-web
```

If you want the XNAT Legacy Pipeline Engine installed, you'll need to clone that as well:

```bash
git clone git@github.com:NrgXnat/xnat-pipeline-engine.git /local/path/to/xnat-pipeline
```

---
> **NOTE**: As of XNAT 1.8._x_, the legacy pipeline engine is no longer required for regular functioning. [The XNAT Container Service plugin](https://wiki.xnat.org/container-service)
is recommended for external data processing tasks going forward. XNAT still supports integration with the XNAT Legacy
pipeline engine for now so that you can continue to use existing pipeline definitions.

---

In this folder (i.e. the **xnat-dev** configuration folder), create a file named `local.yaml`. Specify the **`xnat_src`** option
with the path to the folder containing the XNAT source code (you may want to duplicate the `sample.local.yaml` file and rename it
to `local.yaml` as a starting point):

```
xnat_src:     '/local/path/to/xnat-web'
```

> You **MUST** specify the `xnat_src` parameter before continuing or setup will fail.

If provisioning the pipeline engine, you'll need to add **pipeline_src** to `local.yaml` as well:

```
pipeline_src:     '/local/path/to/pipeline'
```

Optionally, you can mount folders from the **xnat** user's home folder, which is in **/data/xnat/home**. The standard
folders here are:

- **config** contains the XNAT configuration file(s)
- **logs** contains the XNAT application logs
- **plugins** contains any installed XNAT plugin libraries
- **work** contains temporary files for managing downloads, server work, etc.

The **xnat-vagrant** **.gitignore** file contains entries for these folders at the root level of the project, meaning
you can create and mount these folders without them showing up as changes to the source-controlled project. To mount
these folders on your VM, you need to configure them in a **shares** section in your **local.yaml** file. The code
below demonstrates how to configure the **logs** and **plugins** folders.

```bash
shares:
    '../../plugins':
        - '/data/xnat/home/plugins'
        - ['fmode=644','dmode=755']
    '../../logs':
        - '/data/xnat/home/logs'
        - ['fmode=644','dmode=755']
```

> See the [top-level README file](../../README.md) for more information on setting up configuration
 and initialization options through shared folders.

Once you have your local configuration set up, launch the `setup.sh` script (or `setup.bat` on Windows):

```bash
./setup.sh
```

> The setup script automates the process of creating the Vagrant VM and configuring it for development.

You can do the setup manually with the following commands:

```bash
vagrant up
vagrant reload
vagrant provision --provision-with build
```

> This will create a Vagrant VM with XNAT built from the [xnat-web](https://bitbucket.org/xnatdev/xnat-web)
> repo using default settings in the `'config.yaml'` for the config you're building, using the source code
> specified in your `'local.yaml'` file. If you'd like to further customize your installation,
> you may set custom values in your `'local.yaml'` file for properties in `'config.yaml'`.

## Accessing the XNAT application

After setup is complete, log in to your XNAT site:

```
http://192.168.56.100
username: admin
password: admin
```

If you'd like to access the XNAT VM via SSH, run this command from the `'xnat_vagrant'` folder

```bash
vagrant ssh
```

Then switch to the VM user (**`xnat`** in this case) after you're logged in:

```bash
sudo su - xnat
```

## Updating and developing

After your VM is provisioned, you can rebuild your XNAT webapp with Gradle. The README file in the
['xnat-web' repo](https://bitbucket.org/xnatdev/xnat-web) should help with that. The simplest (but slowest)
way to do the Gradle build is from *inside* the VM. To do this, log into the VM with `vagrant ssh` and
run these commands (in this case, 'xnat' is both the `project` and `xnat_src` value):

```bash
cd /data/xnat/src/xnat
./gradlew war deployToTomcat
```

After the setup and build steps are completed, log in to your XNAT site:

```
http://10.1.1.170
username: admin
password: admin
```

> **Note:** After initial setup, just run **`vagrant reload`** from your Vagrant folder to launch your XNAT VM.

If you choose to rebuild the XNAT web application on the host machine, you can deploy the rebuilt application by
shutting down Tomcat then redeploying with the provided `clear-xnat-logs-restart` script.

```bash
$ systemctl stop tomcat8.service
$ clear-xnat-logs-restart -r
```

The `clear-xnat-logs-restart` script does a couple of interesting things:

* Removes all Tomcat logs from `/var/log/tomcat8`
* Removes all XNAT logs from _xnat_home_`/logs`
* Starts Tomcat and monitors the start-up log

The `-r` option adds one additional step, deleting the existing XNAT war file and deployed web application,
replacing it with the war file in _data_root_`/src/xnat/build/libs`.
