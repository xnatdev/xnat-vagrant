# XNAT 1.6.5 Pre-built Vagrant Box

## About the xnat-165 configuration

This configuration downloads and runs a pre-built XNAT 1.6.5 Vagrant box. The version of XNAT running in this box is
the 1.6.5 release, extracted from the official release archive that can be downloaded at
[ftp://ftp.nrg.wustl.edu/pub/xnat/xnat-1.6.5.tar.gz](ftp://ftp.nrg.wustl.edu/pub/xnat/xnat-1.6.5.tar.gz). The XNAT
pipeline is also installed using source from
[ftp://ftp.nrg.wustl.edu/pub/xnat/pipeline-installer-1.6.5.tar.gz](ftp://ftp.nrg.wustl.edu/pub/xnat/pipeline-installer-1.6.5.tar.gz).

To run this VM, simply run `vagrant up` from the `configs/xnat-165` directory or `./run xnat-165 start` from this repo's root
directory. There is a [setup script](setup.sh) included, but it just runs `vagrant up` command since this box is pre-built
and there is no setup needed.

```bash
vagrant up
```

To access the pre-configured site, go to [http://192.168.56.165](http://192.168.56.165) and log in with the username `admin` and password
`admin`. If you get an error stating the site can't be reached, you may need to add the port number `:8080` to the site url in
your browser, changing it to [http://192.168.56.165:8080](http://192.168.56.165:8080), and refresh the page. To prevent this error in the
future, add the port number `:8080` to the `Site URL` setting and save the changes.

The VM has been set up with a local user named `xnat`. You can change this user's password after logging in with the `vagrant ssh`
command (make sure you run the command from this configuration's directory):

```bash
vagrant ssh
```

After logging into the VM, use the `passwd` command to set the password for the `xnat` user.

```bash
sudo passwd xnat
```

Enter the desired password when prompted then exit from the VM.

```bash
exit
```

You should now be able to log into the VM via SSH or SFTP as the `xnat` user (using the new password) at `192.168.56.165`.

```bash
ssh xnat@192.168.56.165
```

## Doing development with this VM

In the VM, the paths to the XNAT and Pipeline source code are, respectively:

- `/data/xnat/src/xnat`
- `/data/xnat/src/pipeline-installer`


You can modify the source there (or copy modified files via SCP or SFTP) then run the `setup.sh`, `update.sh`, or
`quick-deploy*.sh` scripts to push your changes to the running web app at `/var/lib/tomcat7/webapps/xnat`. If you're
doing front-end work, you can make changes to front-end code (Velocity templates, JavaScript, CSS, images, etc.)
directly in the web app and see your changes instantly (after refreshing the page, of course).

## Note

Since this box is pre-built, the initial configuration cannot be changed. The IP address can be changed in [config.yaml](config.yaml),
but if it does not match the IP address that the running XNAT expects (192.168.56.165), you will not be able to use the site. If you need
to change the IP address, do so first in the running XNAT site, then create a file named `local.yaml` in this folder and set the
IP address using the `vm_ip` property:

```
vm_ip: 192.168.0.100
```

Once you've saved the `local.yaml` file, restart the VM:

```bash
vagrant up
```
