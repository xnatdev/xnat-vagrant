#!/usr/bin/env bash

source ../../scripts/setup-macros.sh

checkWorkFolder
createVarsFiles
stackBoxUpdate

#echo $NAME

if [[ ! -z ${NAME} && -e ./.vagrant/machines/${NAME}/virtualbox/index_uuid ]]; then

    echo VM already created...
    #    echo "Run \`vagrant reload $NAME\` to start or restart the VM".
    exit

fi

getConfigValueFromFile() {
    echo $(grep ^$2 $1 | cut -f 2- -d : | sed -e 's/#.*$//' | sed -e 's/^[[:space:]]*//')
}

getConfigValue() {
    local VALUE=$(getConfigValueFromFile local.yaml $1)
    [[ -z ${VALUE} ]] && { VALUE=$(getConfigValueFromFile config.yaml $1); }
    echo ${VALUE}
}

[[ -z ${XNAT_URL} ]] &&
    { XNAT_URL=$(getConfigValue xnat_url) || echo "XNAT_URL found"; }

[[ -z ${PIPELINE_URL} ]] &&
    { PIPELINE_URL=$(getConfigValue pipeline_url) || echo "PIPELINE_URL found"; }

GET_FRESH=Y
doDownload() {
    if [[ -z ${2} ]]; then
        echo Downloading: ${1}
        curl --location --fail --retry 5 --retry-delay 5 --remote-name ${1}
        local STATUS=${?}
    else
        echo Downloading ${1} from URL ${2}
        curl --location --fail --retry 5 --retry-delay 5 --remote-name ${1} ${2}
        local STATUS=${?}
    fi
    [[ ${STATUS} -gt 0 ]] && { echo "Error downloading ${1}"; exit ${STATUS}; }
}

FOUND_FILE=""

# usage:
# findFirst file.txt /path/to/dir1 ./dir2 ./dir3/dir4
# returns first found path (or empty string if not found) as $firstFound var
findFirst() {

    FILENAME=$1

    shift

    # reset $firstFound var
    FOUND_FILE=""
    # return if we find it right away
    if [[ -e ${FILENAME} ]]; then
        FOUND_FILE=${FILENAME}
        return 0
    fi
    # or check the list of directories
    for dir in $@; do
        if [[ -d ${dir} && -e ${dir}/${FILENAME} ]]; then
            FOUND_FILE=${dir}/${FILENAME}
            return 0
        fi
        continue
    done
    return 1
}

# search config folder, then 'local' folder then 'local/downloads'
DL_DIRS="./ ../../local ../../local/downloads"

XNAT_WAR_FILENAME=${XNAT_URL##*/}

findFirst ${XNAT_WAR_FILENAME} ${DL_DIRS}
XNAT_WAR=${FOUND_FILE}

if [[ -e ${XNAT_WAR} ]]; then
    prompt "${XNAT_WAR_FILENAME} has already been downloaded. Would you like to download a new copy? [Y/n]"
    [[ ${?} == 1 ]] && { GET_FRESH_XNAT_WAR=Y; } || { GET_FRESH_XNAT_WAR=N; }
else
    GET_FRESH_XNAT_WAR=Y
fi

[[ ! -z ${PIPELINE_URL} ]] && {
    PIPELINE_URL_FILENAME=${PIPELINE_URL##*/}
    [[ ${PIPELINE_URL_FILENAME} =~ ^[0-9].* ]] && { PIPELINE_ZIP_FILENAME="xnat-pipeline-${PIPELINE_URL_FILENAME}"; } || { PIPELINE_ZIP_FILENAME="${PIPELINE_URL_FILENAME}"; }

    findFirst ${PIPELINE_ZIP_FILENAME} ${DL_DIRS}
    PIPELINE_ZIP=${FOUND_FILE}

    if [[ -e ${PIPELINE_ZIP} ]]; then
        prompt "${PIPELINE_ZIP_FILENAME} has already been downloaded. Would you like to download a new copy? [Y/n]"
        [[ ${?} == 1 ]] && { GET_FRESH_PIPELINE_ZIP=Y; } || { GET_FRESH_PIPELINE_ZIP=N; }
    else
        GET_FRESH_PIPELINE_ZIP=Y
    fi
} || {
        GET_FRESH_PIPELINE_ZIP=N
}

[[ ! ${GET_FRESH_XNAT_WAR} =~ [Nn] ]] && {
    [[ -e ${XNAT_WAR} ]] && { rm ${XNAT_WAR}; }
    echo
    echo Downloading from configured URL: ${XNAT_URL}
    mkdir -p ../../local/downloads/
    cd ../../local/downloads/
    doDownload ${XNAT_URL}
    cd -
}

[[ ! ${GET_FRESH_PIPELINE_ZIP} =~ [Nn] ]] && {
    [[ -e ${PIPELINE_ZIP} ]] && { rm ${PIPELINE_ZIP}; }
    echo
    echo Downloading from configured URL: ${PIPELINE_URL}
    mkdir -p ../../local/downloads/
    cd ../../local/downloads/
    [[ ${PIPELINE_URL_FILENAME} == ${PIPELINE_ZIP_FILENAME} ]] && { doDownload ${PIPELINE_URL}; } || { doDownload ${PIPELINE_ZIP_FILENAME} ${PIPELINE_URL}; }
    cd -
}

echo
echo Starting XNAT build...

echo
echo Provisioning VM with specified user...

echo
vagrant up

echo
echo Reloading VM to configure folder sharing...

echo
vagrant reload

echo
echo Running build provision to build and deploy XNAT on the VM...

echo
vagrant provision --provision-with build

[[ -e ./.vagrant/machines/${NAME}/virtualbox/index_uuid ]] &&
    cp -fv ./.vagrant/machines/${NAME}/virtualbox/index_uuid ./.work/

echo
echo Provisioning completed.
