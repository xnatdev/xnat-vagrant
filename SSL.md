# XNAT Vagrant and SSL

XNAT Vagrant provides the ability to generate self-signed SSL certificates, as well as self-signed SSL certificate _authorities_. The latest versions of many browsers [require HTTPS for lots of features](https://www.digicert.com/blog/https-only-features-in-browsers), including accessing sites via server address.

## Configuration

The provisioning script decides whether SSL should be configured based on the values of the `protocol` and `site_url` settings for your VM (note that neither of these is set in the default VM configurations).

### Non-SSL

If you prefer to use non-SSL addressing, leave these settings to their default values. If you need to specify them in a custom configuration:

* Set `protocol` to `http`
* Set `site_url` to a value that starts with `http://`

With this, you should be able to access your XNAT without SSL or certificate issues.

### SSL

If the provisioning script determines that the server should be configured to use SSL, it checks the value of the following options:

Option | Description
------ | -----------
`ssl_ca_pem` | Specifies a path on the host machine to the PEM certificate for a certificate authority.
`ssl_ca_key` | Specifies a path on the host machine to the private key for the certificate authority indicated by `ssl_ca_pem`.
`ssl_cert` | Specifies a path on the host machine to an SSL certificate.
`ssl_key` | Specifies a path on the host machine to the private key for the SSL certificate indicated by `ssl_cert`.
`ssl_subject` | Specifies the subject to be used when generating a new self-signed certificate authority and SSL certificate

These options are used as follows:

* If `ssl_cert` and `ssl_key` are specified, the files are copied from the host machine to the guest VM and are used when configuring nginx for SSL
* If `ssl_ca_pem` and `ssl_ca_key` are specified, they are stored in the VM's SSL certificate folders and are used to generate a new self-signed certificate and private key, which are then used when configuring nginx for SSL
* If all four are specified, the certificate authority PEM and private key are stored in the VM's certificate folders and the certificate and key are used when configuring nginx for SSL
* If none of these options are specified, a new certificate authority and SSL certificate (along with their private keys) are generated using the subject specified in `ssl_subject`

Note that `ssl_subject` defaults to the value:

```
/C=US/ST=Missouri/L=Saint Louis/O=Washington University School of Medicine/OU=Neuroinformatics Research Group/CN=SERVER
```

`SERVER` is the server name for the VM, so `xnatdev.xnat.org` or the corresponding value for your configuration. You can specify your own subject with the `ssl_subject` setting in your `local.yaml` configuration, e.g.:

```
ssl_subject: /C=US/ST=Massachusetts/L=Arkham/O=Miskatonic University School of Medicine/OU=Reanimation Research Lab/CN=XNAT
```

## Using XNAT with SSL CA and/or certificate

The upside to running XNAT with SSL is that it more actually reflects most production deployment environments: due to security requirements and privacy concerns, most research laboratories and institutions deploy XNAT and similar tools on SSL-secured platforms. The problem is that these certificates are often not accessible to developers working on a local deployment. To address this, XNAT Vagrant provides the ability to generate a self-signed certificate authority and/or SSL certificate. The problem with _this_ is that self-signed certificates are not accepted by browsers by default. You can work around this by explicitly installing a self-signed certificate on your workstation, but that means you have to re-install the SSL certificate every time you rebuild your VM.

> **Note:** This section is primarily intended to document generating and using locally generated certificate authorities and self-signed SSL certificates. However, if you have access to a valid trusted CA-issued SSL certificate, you can use it with XNAT Vagrant, as described [below](#markdown-header-deploying-certificate-authorities-and-ssl-certificates).

XNAT Vagrant addresses these issues by generating both SSL certificates that can be re-used for each VM, as well as the certificate authority that can be used to generate SSL certificates across multiple VMs. The workflow is as follows:

1. Starting with neither a certificate authority or SSL certificate, deploy a new VM with HTTPS for the protocol (as noted above, this means setting `protocol` to `https` and/or setting `site_url` to a value that starts with `https://`). The generated certificate authority and SSL certificate, as well as the accompanying private keys, are copied into the `ssl` folder under your VM configuration folder, e.g. `configs/xnat-dev/ssl`.
1. Once the VM is fully deployed, copy the CA and certificate files somewhere for safe keeping. As noted in the previous step, you can find these files in the `ssl` folder under your VM configuration folder.
1. Configure your system to recognize either the new certificate authority or the SSL certificate, as described in [Installing certificate authorities and SSL certificates](#markdown-header-installing-certificate-authorities-and-ssl-certificates). You _may_ need to restart your browser after installing the certificate authority or SSL certificate.
1. With the certificate authority or SSL certificate installed on your system, try to access your XNAT using the site URL with HTTPS, e.g. https://xnatdev.xnat.org. If everything is properly configured, your browser should connect to the site and display the URL as a safe and secured HTTPS site.
1. Now when deploying a new VM with XNAT Vagrant, you can add the `ssl_ca_pem` and `ssl_ca_key` options to your `local.yaml` file. The values for these should be a path to the CA PEM and private key files. The path should be either absolute or relative to the _configuration_ folder, e.g. relative to `configs/xnat-dev` rather than the root folder for XNAT Vagrant. XNAT Vagrant generates a new SSL certificate and private key from the certificate authority. The new SSL certificate should work the same as the previous SSL certificate as long as you configured your system to recognize the certificate authority. Alternatively, you can add the `ssl_cert` and `ssl_key` options to `local.yaml`, with the same results but with the caveat that it will only work for VMs with the same server address, i.e. you can't re-use your certificate generated for `xnatdev.xnat.org` on a VM with the address `release.xnat.org`.

### Installing certificate authorities and SSL certificates

The process for installing certificates depends on your host machine's operating system:

#### Windows

_TBD_

#### OS X

Installing a certificate authority or SSL certificate on OS X is the same regardless of which you're installing. There are two ways to do this.

The first uses the command line. In a terminal window or shell, run the following command:

```
sudo security add-trusted-cert -d -k /Library/Keychains/System.keychain <path-to-pem-or-crt-file>
```

You'll need to enter your password a couple of times during this process: once on the command line for the `sudo` command and once in a pop-up
dialog as the certificate is imported and set to trusted in your keychain.

The second way to install the certificate authority or SSL certificate uses the Keychain Access application.

1. Open the Keychain Access application
1. Click *File -> Import Items...*
1. Browse to the location of the certificate you want to import
1. Select the certificate and click *Open*
1. Right-click the newly imported certificate entry, then click *Get Info*
1. Expand the *Trust* section of the certificate dialog
1. For the top entry labeled *When using this certificate:*, click the drop-down list and select *Always Trust*
1. Close the certificate dialog (a prompt dialog labeled *You are making changes to your Certificate Trust Settings* should pop up)
1. Enter your password in the prompt

Your certificate authority or SSL certificate should now be recognized as valid by browsers on your host machine.

#### Linux

_TBD_

### Deploying certificate authorities and SSL certificates

Once you've installed a certificate authority and/or SSL certificate, you can re-use it when creating new XNAT Vagrant VMs.

To re-use a previously generated certificate authority, add the `ssl_ca_pem` and `ssl_ca_key` options to your `local.yaml` file. XNAT Vagrant generates a new SSL certificate and private key from the certificate authority. The new SSL certificate should work the same as the previous SSL certificate as long as you configured your system to recognize the certificate authority.

To re-use a previously generated SSL certificate, add the `ssl_cert` and `ssl_key` options to `local.yaml`. This should work the same as before with the caveat that it will _only_ work for VMs with the same server address, i.e. you can't re-use your certificate generated for `xnatdev.xnat.org` on a VM with the address `release.xnat.org`. You can however

 The values for these should be a path to the CA PEM and private key files. The path should be either absolute or relative to the _configuration_ folder, e.g. relative to `configs/xnat-dev` rather than the root folder for XNAT Vagrant.  Alternatively, you can
